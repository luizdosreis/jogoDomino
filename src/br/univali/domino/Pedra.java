/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.univali.domino;

/**
 *
 * @author luizhenrique
 */
public class Pedra {

    private Integer lado0;
    private Integer lado1;

    /**
     *
     * @return o total de pontos que a pedra possui
     */
    public int pegaPontos() {
        return this.getLado0() + this.getLado1();
    }

    /**
     * verifica se o this e maior que o parametro
     *
     * @param pedra
     * @return true se for maior
     */
    public Boolean ehMaiorQue(Pedra pedra) {
        if (this.ehDob() && pedra.ehDob()) {
            return this.getLado0() > pedra.getLado0();
        }
        if (this.ehDob()) {
            return true;
        }
        if (pedra.ehDob()) {
            return false;
        }
        if (this.pegaMaiorLado().intValue() > pedra.pegaMaiorLado().intValue()) {
            return true;
        }
        return this.pegaMenorLado().intValue() > pedra.pegaMenorLado().intValue();
    }

    /**
     * 
     * @param lado
     * @return Retorna true se a pedra possuir um dos lados igual ao lado passado por parametro.
     */
    public boolean temLado(int lado) {
        return this.getLado0() == lado || this.getLado1() == lado;
    }

    /**
     *
     * @return o maior lado da pedra
     */
    public Integer pegaMaiorLado() {
        if (this.getLado0() > this.getLado1()) {
            return this.getLado0();
        }
        return this.getLado1();
    }

    /**
     *
     * @return o menor lado da pedra
     */
    public Integer pegaMenorLado() {
        if (this.getLado0() < this.getLado1()) {
            return this.getLado0();
        }
        return this.getLado1();
    }

    /**
     *
     * @return se os dois lados sao iguais retorna true
     */
    public Boolean ehDob() {
        return getLado0().intValue() == getLado1().intValue();
    }

    /**
     * Inverte os lados da pedra para poder encaixar
     *
     */
    public void inverteOsLados() {
        Integer auxiliar = getLado0();
        setLado0(getLado1());
        setLado1(auxiliar);
    }

     /**
     * Se a showPedra = true, apresenta o lados da pedra, senão, apresenta um X no lugar do valor de cada lado ([X,X]).
     * @param showPedra
     * @return Uma string apresentando a estrutura da pedra. Formato: [1,2]
     */
    public String montaPedraString(Boolean showPedra) {
        if (showPedra) {
            return this.toString();
        } else {
            return "[X,X]";
        }
    }

    @Override
    public String toString() {
        return "[" + this.getLado0() + "," + this.getLado1() + "]";
    }

    /**
     * @return the lado0
     */
    public Integer getLado0() {
        return lado0;
    }

    /**
     * @param lado0 the lado0 to set
     */
    public void setLado0(Integer lado0) {
        this.lado0 = lado0;
    }

    /**
     * @return the lado1
     */
    public Integer getLado1() {
        return lado1;
    }

    /**
     * @param lado1 the lado1 to set
     */
    public void setLado1(Integer lado1) {
        this.lado1 = lado1;
    }

  
    
}

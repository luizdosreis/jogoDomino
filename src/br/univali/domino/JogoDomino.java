/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.univali.domino;

import br.univali.poo.io.ConsoleRead;
import br.univali.poo.io.ConsoleWrite;
import java.util.ArrayList;

/**
 *
 * @author luizhenrique
 */
public class JogoDomino {

    private ArrayList<Jogador> jogadores;
    private Monte monte;
    private Tabuleiro tabuleiro;
    private int opcaoSelecionada, jogadorDaVez, qtdPassada, rodada;
    private boolean flJogoAcabou, flTemHumano;

    /**
     * Constructor
     */
    public JogoDomino() {

    }

    /**
     * Inicia o jogo
     */
    public void executarJogo() {

        jogadores = new ArrayList<>();
        monte = new Monte();
        tabuleiro = new Tabuleiro();
        flJogoAcabou = false;
        qtdPassada = 0;
        rodada = 1;
        ConsoleWrite.printComQuebraDeLinha("Bem-vindo ao jogo!!");
        menu();
        criaJogadores();
        sorteiaPedras();
        definePrimeiroJogador();
        jogadas();
        verificaVencedor();
    }

    /**
     * Controla as jogadas durante o jogo. Verifica se o jogo acabou ou não.
     */
    private void jogadas() {
        int numeroEsquerda, numeroDireita;
        do {
            rodada++;
            proximoJogador();
            numeroEsquerda = tabuleiro.pegaNumeroPontaEsquerda();
            numeroDireita = tabuleiro.pegaNumeroPontaDireita();
            if (pegaJogadorDaVez().isBot()) {
                jogadaBot(numeroEsquerda, numeroDireita);
            } else {
                jogadaHumano(numeroEsquerda, numeroDireita);
            }
            verificaJogoAcabou();
        } while (!flJogoAcabou);
    }

    
    /**
     * Verifica qual foi o vencedor do jogo, de acordo com as seguintes regras.
     * Vence o jogo o jogador que não possui nenhuma pedra na mão ou o jogador que possuir menos pontos no somatório das pedras.
     */
    public void verificaVencedor() {
        Jogador jogador;
        ConsoleWrite.printComQuebraDeLinha("FIM DE JOGO!!");
        if (!pegaJogadorDaVez().possuiPedra()) {
            ConsoleWrite.printComQuebraDeLinha("O jogador vencedor foi: " + pegaJogadorDaVez().getNome() + ", com nenhuma pedra na mão!");
        } else {
            jogador = jogadores.get(0);
            for (int i = 1; i < jogadores.size(); i++) {
                if (jogadores.get(i).totalPontosPedras() < jogador.totalPontosPedras()) {
                    jogador = jogadores.get(i);
                }
            }
            ConsoleWrite.printComQuebraDeLinha("O jogador vencedor foi :" + jogador.getNome() + " com " + jogador.totalPontosPedras() + " pontos!");
        }
    }

    /**
     * Apresenta no console o tabuleiro e a mão do jogador humano.
     * Solicita uma pedra para jogar no tabuleiro e verifica se a pedra é válida.
     * 
     * @param numeroEsquerda
     * @param numeroDireita 
     */
    private void jogadaHumano(int numeroEsquerda, int numeroDireita) {
        int posicaoPedra;
        boolean flJogadaFeita = false;
        Pedra pedra = null;

        if (pegaJogadorDaVez().pegaPedraPeloLado(numeroEsquerda) != null || pegaJogadorDaVez().pegaPedraPeloLado(numeroDireita) != null) {
            do {
                ConsoleWrite.printComQuebraDeLinha(pegaJogadorDaVez().montaPedraMaoString());
                posicaoPedra = ConsoleRead.readlnInt("Qual pedra você deseja jogar?");
                ConsoleWrite.printSemQuebrarLinha("\n\n");
                posicaoPedra--;
                pedra = pegaJogadorDaVez().pegaPedraPeloIndice(posicaoPedra);
                if (verificaSeEncaixaNaPontaEsquerda(pedra)) {
                    tabuleiro.inserePedraPontaEsquerda(pedra);
                    pegaJogadorDaVez().removePedra(pedra);
                    flJogadaFeita = true;
                } else if (verificaSeEncaixaNaPontaDireita(pedra)) {
                    tabuleiro.inserePedraPontaDireita(pedra);
                    pegaJogadorDaVez().removePedra(pedra);
                    flJogadaFeita = true;
                }

                if (!flJogadaFeita) {
                    ConsoleWrite.printComQuebraDeLinha("A pedra " + pedra + " não encaixa no tabuleiro!");
                }
            } while (!flJogadaFeita);
        } else {
            pedra = pegaMonte(numeroEsquerda, numeroDireita);
            jogaDoMonte(pedra);
        }

        if (pedra != null) {
            qtdPassada = 0;
        }
        rodadaMensagem(pedra);
        pegaJogadorDaVez().tranferePedrasCompradas();

    }

    
    /**
     * Realiza a jogada para o jogador bot.
     * @param numeroEsquerda
     * @param numeroDireita 
     */
    private void jogadaBot(int numeroEsquerda, int numeroDireita) {
        Pedra pedra = pegaJogadorDaVez().pegaPedraPeloLado(numeroEsquerda);
        if (pedra != null) {
            verificaSeEncaixaNaPontaEsquerda(pedra);
            tabuleiro.inserePedraPontaEsquerda(pedra);
            pegaJogadorDaVez().removePedra(pedra);
        } else {
            pedra = pegaJogadorDaVez().pegaPedraPeloLado(numeroDireita);
            if (pedra != null) {
                verificaSeEncaixaNaPontaDireita(pedra);
                tabuleiro.inserePedraPontaDireita(pedra);
                pegaJogadorDaVez().removePedra(pedra);
            } else {
                pedra = pegaMonte(numeroEsquerda, numeroDireita);
                jogaDoMonte(pedra);

            }
        }
        if (pedra != null) {
            qtdPassada = 0;
        }
        rodadaMensagem(pedra);
        pegaJogadorDaVez().tranferePedrasCompradas();
    }

    /**
     * Verifica se o jogo acabou, de acordo com as seguintes regras:
     * 1. O jogador da vez não possui nenhuma pedra na mão.
     * 2. Todos os jogadores passaram a vez.
     */
    private void verificaJogoAcabou() {
        flJogoAcabou = !pegaJogadorDaVez().possuiPedra() || qtdPassada == jogadores.size();

    }

    /**
     * Realiza o emprestimo de pedra da lista de monte e verifica se a pedra possui um dos lados do tabuleiro. Caso não se encaixe, empresta outra pedra até achar uma que se encaixe.
     * @param numeroEsquerda
     * @param numeroDireita
     * @return A pedra que se encaixa no tabuleiro ou nulo caso o monte não possua nenhuma pedra.
     */
    private Pedra pegaMonte(int numeroEsquerda, int numeroDireita) {
        boolean flAchouPedra = false;
        Pedra pedra = null;
        while (!flAchouPedra && !monte.estaVazia()) {
            Pedra pedraEmprestado = monte.pegaEmprestado();
            pegaJogadorDaVez().addPedraComprada(pedraEmprestado);
            if (pedraEmprestado.temLado(numeroEsquerda) || pedraEmprestado.temLado(numeroDireita)) {
                flAchouPedra = true;
                pedra = pedraEmprestado;
            }

        }
        return pedra;
    }

    /**
     * Verifica em qual lado a pedra se encaixa no tabuleiro.
     * @param pedraMonte 
     */
    private void jogaDoMonte(Pedra pedraMonte) {
        if (pedraMonte != null) {
            if (verificaSeEncaixaNaPontaEsquerda(pedraMonte)) {
                tabuleiro.inserePedraPontaEsquerda(pedraMonte);
            } else if (verificaSeEncaixaNaPontaDireita(pedraMonte)) {
                tabuleiro.inserePedraPontaDireita(pedraMonte);
            }
        } else {
            qtdPassada++;
        }

    }

    /**
     * Incrementa o atributo proximoJogador
     */
    private void proximoJogador() {
        if (jogadorDaVez < jogadores.size() - 1) {
            jogadorDaVez++;
        } else {
            jogadorDaVez = 0;
        }
    }

    /**
     * Pega o jogador da lista de jogadores
     *
     * @return jogador da posica jogadorDaVez na lista de jogadores
     */
    private Jogador pegaJogadorDaVez() {
        return jogadores.get(jogadorDaVez);
    }

    /**
     * Escreve o menu no console e solicita que uma opção seja selecionada;
     */
    private void menu() {
        boolean flInvalida;
        do {
            ConsoleWrite.printComQuebraDeLinha("Opcao 1: Com jogador Humano");
            ConsoleWrite.printComQuebraDeLinha("Opcao 2: Somente com bots");
            this.opcaoSelecionada = ConsoleRead.readlnInt("Informe a opcão desejada ");
            flInvalida = this.opcaoSelecionada < 1 || this.opcaoSelecionada > 2;
            if (flInvalida) {
                ConsoleWrite.printComQuebraDeLinha("Opcao invalida, digite novamente!");
            }
        } while (flInvalida);
    }

    /**
     * Cria a lista de jogadores de acordo com a opção selecionada na tela;
     *
     */
    private void criaJogadores() {
        int opcao, quantidadeDeBots;
        boolean flInvalida;
        flTemHumano = this.opcaoSelecionada == 1;

        if (flTemHumano) {
            jogadores.add(new Jogador(ConsoleRead.readLine("Informe o nome do jogador humano: "), false));
        }

        do {
            quantidadeDeBots = ConsoleRead.readlnInt("Informe a quantidade de bots (max = 3 com humano e max = 4 sem humano)");
            flInvalida = (flTemHumano && (quantidadeDeBots < 1 || quantidadeDeBots > 3)) || (!flTemHumano && (quantidadeDeBots < 2 || quantidadeDeBots > 4));
            if (flInvalida) {
                ConsoleWrite.printComQuebraDeLinha("Opcao invalida, digite novamente!");
            }
        } while (flInvalida);

        for (int i = 0; i < quantidadeDeBots; i++) {
            jogadores.add(new Jogador(ConsoleRead.readLine("Informe o nome do jogador bot " + (i + 1) + ": "), true));
        }
    }

    /**
     * Distribui 7 pedras para cada jogador;
     */
    private void sorteiaPedras() {
        for (Jogador jogador : jogadores) {
            for (int i = 0; i < 7; i++) {
                jogador.addPedra(monte.pegaEmprestado());
            }
        }
    }

    /**
     * Define qual jogador deve começar jogando, de acordo com as seguintes regras.
     * 1. O jogador que possuir o maior dob, na sequencia: [6,6],[5,5], [4,4],[3,3],[2,2],[1,1],[0,0]
     * 2. O jogador que possuir a maior pedra.
     */
    private void definePrimeiroJogador() {
        jogadorDaVez = 0;
        for (int i = 1; i < jogadores.size(); i++) {
            if (jogadores.get(i).pegaMaiorPedra().ehMaiorQue(jogadores.get(jogadorDaVez).pegaMaiorPedra())) {
                jogadorDaVez = i;
            }
        }
        Pedra pedra = jogadores.get(jogadorDaVez).pegaMaiorPedra();
        tabuleiro.inserePedraPontaEsquerda(pedra);
        jogadores.get(jogadorDaVez).removePedra(pedra);
        rodadaMensagem(pedra);
    }

    /**
     * verifica se a pedra encaixa na ponta esquerda
     *
     * @param pedra
     * @return true se encaixa e false senao encaixa
     */
    private boolean verificaSeEncaixaNaPontaEsquerda(Pedra pedra) {
        if (pedra.getLado1().intValue() == tabuleiro.getPedraPeloIndice(0).getLado0().intValue()) {
            return true;
        }
        if (pedra.getLado0().intValue() == tabuleiro.getPedraPeloIndice(0).getLado0().intValue()) {
            pedra.inverteOsLados();
            return true;
        }
        return false;
    }

    /**
     * verifica se a pedra encaixa na ponta direita
     *
     * @param pedra
     * @return true se encaixa e false senao encaixa
     */
    private boolean verificaSeEncaixaNaPontaDireita(Pedra pedra) {
        if (pedra.getLado0().intValue() == tabuleiro.getPedraPeloIndice(tabuleiro.tamanhoDaLista() - 1).getLado1().intValue()) {
            return true;
        }
        if (pedra.getLado1().intValue() == tabuleiro.getPedraPeloIndice(tabuleiro.tamanhoDaLista() - 1).getLado1().intValue()) {
            pedra.inverteOsLados();
            return true;
        }
        return false;
    }

    /**
     * Printa no console o estado do jogo na rodada corrente.
     * Formato:
     * Rodada: 1
     * Jogador: ver metodo Jogador.montaJogadorString(boolean)
     * Pedra usada: [6,6]
     * Tabuleiro: [6,6]
     * Monte:
     * Obs. O monte somente eh apresentado se não existir nenhum jogador humano no jogo.
     * @param pedra 
     */
    public void rodadaMensagem(Pedra pedra) {
        String msg = "";
        msg += "Rodada: " + rodada + "\n";
        msg += pegaJogadorDaVez().montaJogadorString(!flTemHumano);

        if (pedra != null) {
            msg += "Pedra usada: " + pedra.toString() + "\n";
        } else {
            msg += "Pedra usada: passou a vez!\n";
        }

        msg += tabuleiro.toString();
        if (!flTemHumano) {
            msg += monte.toString();
        }
        ConsoleWrite.printSemQuebrarLinha(msg);
        ConsoleWrite.printSemQuebrarLinha("\n\n");
    }

}

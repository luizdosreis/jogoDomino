/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.univali.domino;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author luizhenrique
 */
public class Monte {

    /**
     * lista com as pedras que nao estao nem com os jogadores nem no tabuleiro
     */
    private ArrayList<Pedra> pedras;

    /**
     * Cria o monte e ja embaralha
     */
    public Monte() {
        pedras = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            for (int j = i; j < 7; j++) {
                Pedra pedra = new Pedra();
                pedra.setLado0(i);
                pedra.setLado1(j);
                pedras.add(pedra);
            }
        }
        embaralhar();
    }

    /**
     * Embaralha o monte
     */
    private void embaralhar() {
        Collections.shuffle(pedras);
    }

    /**
     * pega a pedra do monte pelo seu indice na lista
     *
     * @param indice
     * @return
     */
    public Pedra getPedraPeloIndice(int indice) {
        return pedras.get(indice);
    }

    /**
     * retorna o tamanho da lista que esta no monte
     *
     * @return
     */
    public Integer tamanhoLista() {
        return pedras.size();
    }

    /**
     * retorna a primeira pedra da lista monte
     *
     * @return
     */
    public Pedra pegaEmprestado() {
        Pedra pedra = pedras.get(0);
        pedras.remove(0);
        return pedra;
    }

    /**
     * retorna se a lista do monte esta vazia
     *
     * @return
     */
    public boolean estaVazia() {
        return pedras.isEmpty();
    }

    @Override
    public String toString() {
        return "Monte: " + pedras.toString() + "\n";
    }

}

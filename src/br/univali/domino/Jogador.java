/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.univali.domino;

import java.util.ArrayList;

/**
 *
 * @author CaioGaspar
 */
public class Jogador {

    private String nome;
    private Boolean flBot;
    private ArrayList<Pedra> pedrasMao;
    private ArrayList<Pedra> pedrasCompradas;

    /**
     * Constructor
     * @param nome 
     * @param flBot 
     */
    public Jogador(String nome, Boolean flBot) {
        this.nome = nome;
        this.flBot = flBot;
        this.pedrasMao = new ArrayList<>();
        this.pedrasCompradas = new ArrayList<>();
    }

    /**
     * Add uma pedra na lista de pedrasMao
     *
     * @param pedra
     */
    public void addPedra(Pedra pedra) {
        this.pedrasMao.add(pedra);
    }

    /**
     * Add uma pedra na lista de pedrasCompradas
     *
     * @param pedra
     */
    public void addPedraComprada(Pedra pedra) {
        this.pedrasCompradas.add(pedra);
    }

    /**
     * Transfere toda a lista de pedrasCompradas para a lista de pedrasMao.
     *
     * 
     */
    public void tranferePedrasCompradas() {
        this.pedrasMao.addAll(pedrasCompradas);
        pedrasCompradas = new ArrayList<>();
    }

    /**
     *
     * @return quantidade de pedrasMao que o jogador possui
     */
    public int tamanhoQtdePedras() {
        return this.pedrasMao.size();
    }

    /**
     * Remove a pedra do indice passado por parametro
     *
     * @param indice
     */
    public void removePedraPeloIndice(int indice) {
        this.pedrasMao.remove(indice);
    }

    /**
     * Pega a pedra da lista pedrasMao pelo indice na lista
     * @param indice
     * @return 
     */
    public Pedra pegaPedraPeloIndice(int indice) {
        return this.pedrasMao.get(indice);
    }

    /**
     * Remove a pedra do parametro da lista de pedrasMao
     *
     * @param pedra
     */
    public void removePedra(Pedra pedra) {
        this.pedrasMao.remove(pedra);
    }

    /**
     * Soma o total de pontos da lista de pedrasMao.
     *
     * @return
     */
    public int totalPontosPedras() {
        int total = 0;
        for (Pedra pedra : this.pedrasMao) {
            total += pedra.pegaPontos();
        }

        return total;
    }

    /**
     * Busca na lista de pedra a maior pedra de acordo com a regra do metodo
     * Pedra.ehMaiorQue(Pedra)
     *
     * @return maior Pedra
     */
    public Pedra pegaMaiorPedra() {
        Pedra maiorPedra = this.pedrasMao.get(0);
        for (int i = 1; i < this.tamanhoQtdePedras(); i++) {
            if (this.pedrasMao.get(i).ehMaiorQue(maiorPedra)) {
                maiorPedra = this.pedrasMao.get(i);
            }
        }
        return maiorPedra;
    }

    /**
     * 
     * @param lado
     * @return Retorna a primeira pedra que possuir um dos lados igual ao lado passado por parametro. Retorna nulo se não existir nenhuma pedra.
     */
    public Pedra pegaPedraPeloLado(int lado) {
        for (Pedra pedra : this.pedrasMao) {
            if (pedra.temLado(lado)) {
                return pedra;
            }
        }
        return null;
    }

    /**
     * retorna se a lista de pedra possui elementos
     *
     * @return boolean
     */
    public boolean possuiPedra() {
        return !pedrasMao.isEmpty();
    }

    /**
     * Se a showPedra = true, apresenta o lados da pedra, senão, apresenta um X no lugar do valor de cada lado ([X,X]).
     * @param showPedras
     * @return Retorna a estrutura do Jogador com as informações Nome, Mão e Compradas. 
     * Formato: 
     * Jogador: João
     * Mão: [1,2], [6,6]
     * Compradas: [2,2], [3,4]
     */
    public String montaJogadorString(Boolean showPedras) {
        String msg = "";
        msg += "Jogador :" + nome + "\n";
        msg += "Mão :" + montaPedraString(this.pedrasMao, !isBot() || showPedras) + "\n";
        msg += "Compradas :" + montaPedraString(this.pedrasCompradas, !isBot() || showPedras) + "\n";
        return msg;
    }

    /**
     * 
     * @return Uma string apresentando todas as pedras que estão na lista de pedrasMao. Formato: Mão: [1,2], [6,6]
     */
    public String montaPedraMaoString() {
        return "Mão: " + montaPedraString(this.pedrasMao, true);
    }

    /**
     * Se a showPedra = true, apresenta o lados da pedra, senão, apresenta um X no lugar do valor de cada lado ([X,X]).
     * @param pedras
     * @param showPedra
     * @return Uma string apresentando todas as pedras que estão na lista de pedrasMao. Formato: [1,2], [6,6]
     */
    public String montaPedraString(ArrayList<Pedra> pedras, Boolean showPedra) {
        String msg = "";
        for (Pedra pedra : pedras) {
            msg += pedra.montaPedraString(showPedra);
        }
        return msg;
    }

    /**
     *
     * @return true se o jogador for um bot
     */
    public Boolean isBot() {
        return this.flBot;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

}

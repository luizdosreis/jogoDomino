/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.univali.domino;

import java.util.ArrayList;

/**
 *
 * @author luizhenrique
 */
public class Tabuleiro {

    /**
     * lista com as pedras que foram jogadas
     */
    private ArrayList<Pedra> pedras;

    /**
     * Cria a lista de pedras dentro do tabuleiro
     */
    public Tabuleiro() {
        pedras = new ArrayList<>();
    }

    /**
     * 
     * @return O valor do lado0 da primeira pedra da lista de pedras.
     */
    public int pegaNumeroPontaEsquerda() {
        return pedras.get(0).getLado0();
    }

    /**
     * 
     * @return O valor do lado1 da ultima pedra da lista de pedras.
     */
    public int pegaNumeroPontaDireita() {
        return pedras.get(pedras.size() - 1).getLado1();
    }

    /**
     * insere a pedra na ponta esquerda;
     *
     * @param pedra
     */
    public void inserePedraPontaEsquerda(Pedra pedra) {
        pedras.add(0, pedra);
    }

    /**
     * insere a pedra na ponta direita;
     *
     * @param pedra
     */
    public void inserePedraPontaDireita(Pedra pedra) {
        pedras.add(pedra);
    }

    /**
     * retorna o tamanho da lista
     *
     * @return
     */
    public Integer tamanhoDaLista() {
        return pedras.size();
    }

    @Override
    public String toString() {
        return "Tabuleiro : " + this.pedras + "\n";
    }

    /**
     * pega a pedra pelo indice
     *
     * @param indice
     * @return pedra
     */
    public Pedra getPedraPeloIndice(int indice) {
        return pedras.get(indice);
    }

}
